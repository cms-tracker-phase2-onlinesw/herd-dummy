# Example plugin for the HERD application

This repository contains a few classes that illustrate how to create a hardware-specific/application-specific SWATCH plugin for use by an on-board control application in the phase-2 upgrades. Specifically, it contains an example device class, `DummyDevice`, that:

 * registers 5 commands, implemented by the `AlignLinks`, `ConfigureRxMGTs`, `ConfigureTxMGTs`, `Reboot` and `Reset` classes; and
 * registers an example Finite State Machine (FSM), whose transitions consist of one or more of the commands, run in sequence


## Dependencies

The plugin library has two main dependencies:
 * [SWATCH](https://gitlab.cern.ch/cms-cactus/core/swatch)
 * [HERD](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-app/)


## Build instructions

To build the plugin, firstly clone this repository:
```sh
git clone ssh://git@gitlab.cern.ch:7999/cms-tracker-phase2-onlinesw/herd-dummy.git
```

The easiest way to build the plugin is inside a docker container, using the
[VSCode Dev Containers extension](https://code.visualstudio.com/docs/devcontainers/containers).
This repository includes a VSCode configuration file (`.devcontainer/devcontainer.json`) which
allows one to build in a development container that already contains all dependencies, along with
relevant tooling like git. To use this setup you just need to open the path for the cloned
repository in VSCode, click on the green bar in the bottom left of the window and then click
"Reopen in container". Instructions for other development environments can be found
[here](https://cms-l1t-phase2.docs.cern.ch/online-sw/plugin-development/development-environment.html).

After VSCode has downloaded & started the development container (or other development environment set
up), to build the plugin simply follow the standard CMake build workflow in the VScode terminal:
```sh
mkdir build
cd build
cmake3 ..
make -j$(nproc)
```
