
#include "dummy/swatch/TxPort.hpp"


#include "dummy/Controller.hpp"
#include "dummy/OpticsController.hpp"
#include "dummy/TxMGTStatus.hpp"

#include "swatch/core/MetricConditions.hpp"


namespace dummy {
namespace swatch {

using namespace ::swatch;


TxPort::TxPort(const size_t i, const Controller& aController, const phase2::ChannelID& aConnectedOpticsChannel) :
  OutputPort(i, createId(i), aConnectedOpticsChannel),
  mController(aController),
  mPolarity(registerProperty<std::string>("Polarity")),
  mMgtType(registerProperty<std::string>("MGT type")),
  mRefClockIndex(registerProperty<uint16_t>("Ref clock index")),
  mAmplitude(registerProperty<float>("Diff swing")),
  mMetricQpllLocked(registerMetric<bool>("QpllLocked", "QPLL locked", core::EqualCondition<bool>(false)))
{
  mAmplitude.setUnit("mV");
  mAmplitude.setFormat(core::format::kFixedPoint, 1);
}


TxPort::~TxPort()
{
}


OpticsController* TxPort::getOpticsController()
{
  return OpticsController::get(getConnection()->component);
}


std::string TxPort::createId(const size_t aIndex)
{
  std::ostringstream sstream;
  sstream << "Tx" << std::setw(2) << std::setfill('0') << aIndex;
  return sstream.str();
}


void TxPort::retrievePropertyValues()
{
  set<std::string>(mMgtType, getIndex() < 20 ? "GTH" : "GTY");
  set<std::string>(mPolarity, (getIndex() % 2) == 0 ? "Inverted" : "Normal");
  set<uint16_t>(mRefClockIndex, getIndex() / 8);
  set<float>(mAmplitude, 700 + (getIndex() / 5));
}


void TxPort::retrieveMetricValues()
{
  const auto statusRegisters = mController.getTxPortStatus(getIndex());

  setMetric(mMetricIsOperating, statusRegisters.isOperating);
  setMetric(mMetricQpllLocked, statusRegisters.qpllLocked);
}


} // namespace swatch
} // namespace dummy
