#include "dummy/MonDeadTime.hpp"

#include <chrono>
#include <thread>

bool dummy::mondeadtimeconfig::isFastUpdate()
{
  if (const char* env_p = std::getenv("DUMMY_METRIC_FASTUPDATE")) {
    return std::string(env_p) == "1";
  }
  return false;
}

void dummy::mondeadtimeconfig::monitoring_update_sleep(const DeadTime<float>& dtrange, std::mt19937& rg)
{
  std::uniform_real_distribution<float> randgensleeptimeinNS(dtrange.min, dtrange.max);
  unsigned int sleeptimeinNS = static_cast<unsigned int>(randgensleeptimeinNS(rg));
  std::this_thread::sleep_for(std::chrono::nanoseconds(sleeptimeinNS));
  return;
}