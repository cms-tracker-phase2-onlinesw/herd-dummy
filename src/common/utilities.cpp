
#include "dummy/swatch/utilities.hpp"


#include <algorithm>
#include <ctype.h>
#include <stdexcept>


namespace dummy {
namespace swatch {
namespace utilities {

const std::string kLogicalSlotEnvVar("DUMMY_SWATCH_LOGICAL_SLOT");
const std::string kShelfAddressEnvVar("DUMMY_SWATCH_SHELF_ADDRESS");


bool isCardInShelf()
{
  return std::getenv(kShelfAddressEnvVar.c_str()) and std::getenv(kLogicalSlotEnvVar.c_str());
}


std::string getShelfAddress()
{
  if (const char* lShelfAddress = std::getenv(kShelfAddressEnvVar.c_str()))
    return lShelfAddress;
  else
    throw std::runtime_error("Shelf address is not defined (env var " + kShelfAddressEnvVar + " not set)");
}


uint16_t getLogicalSlot()
{
  if (const char* lLogicalSlotPtr = std::getenv(kLogicalSlotEnvVar.c_str())) {
    const std::string lLogicalSlotStr(lLogicalSlotPtr);
    if (lLogicalSlotStr.find_first_not_of("0123456789") != std::string::npos)
      throw std::runtime_error("Value of " + kLogicalSlotEnvVar + " env variable ('" + lLogicalSlotStr + "') contains invalid characters");

    const size_t lLogicalSlot(std::stoul(lLogicalSlotStr));
    if (lLogicalSlot > 14)
      throw std::runtime_error("Value of " + kLogicalSlotEnvVar + " env variable ('" + lLogicalSlotStr + "') is too large (max value is 14)");

    return lLogicalSlot;
  }
  else
    throw std::runtime_error("Logical slot is not defined (env var " + kLogicalSlotEnvVar + " not set)");
}


std::vector<size_t> parseListOfIndices(const std::string& aList)
{
  if (aList == "none")
    return {};

  std::vector<size_t> lResult;

  size_t lItemStartPosition = 0;
  size_t lNextCommaPosition = 0;

  while ((lItemStartPosition < aList.size()) and (lNextCommaPosition != std::string::npos)) {
    lNextCommaPosition = aList.find(',', lItemStartPosition);
    const size_t lDashPosition = std::min(aList.find('-', lItemStartPosition), lNextCommaPosition);

    if (lNextCommaPosition == lItemStartPosition)
      throw std::runtime_error("Index list \"" + aList + "\" has invalid syntax: Number missing before comma (at character " + std::to_string(lNextCommaPosition) + ")");

    // Single number
    if (lDashPosition == lNextCommaPosition) {
      const std::string lItem(aList.substr(lItemStartPosition, lNextCommaPosition - lItemStartPosition));
      if (not std::all_of(lItem.begin(), lItem.end(), isdigit))
        throw std::runtime_error("Index list \"" + aList + "\" has invalid syntax: Item \"" + lItem + "\" contains unexpected characters");

      lResult.push_back(std::stoi(lItem));
    }
    else {
      if ((lDashPosition == lItemStartPosition) or (lDashPosition == (lNextCommaPosition - 1)))
        throw std::runtime_error("Index list \"" + aList + "\" has invalid syntax: Item \"" + aList.substr(lItemStartPosition, lNextCommaPosition - lItemStartPosition) + "\" starts/ends with a dash");

      const std::string lPart1(aList.substr(lItemStartPosition, lDashPosition - lItemStartPosition));
      const std::string lPart2(aList.substr(lDashPosition + 1, lNextCommaPosition - lDashPosition - 1));
      if (not std::all_of(lPart1.begin(), lPart1.end(), isdigit) or not std::all_of(lPart2.begin(), lPart2.end(), isdigit))
        throw std::runtime_error("Index list \"" + aList + "\" has invalid syntax: Item \"" + aList.substr(lItemStartPosition, lNextCommaPosition - lItemStartPosition) + "\" contains unexpected characters");

      const size_t lIndex1 = std::stoi(lPart1);
      const size_t lIndex2 = std::stoi(lPart2);
      if (lIndex1 >= lIndex2)
        throw std::runtime_error("Index list \"" + aList + "\" has invalid syntax: Second index in item \"" + aList.substr(lItemStartPosition, lNextCommaPosition - lItemStartPosition) + "\" is not greater than first index");

      for (size_t i = lIndex1; i <= lIndex2; i++)
        lResult.push_back(i);
    }

    lItemStartPosition = lNextCommaPosition + 1;
  }

  return lResult;
}


}
}
}
