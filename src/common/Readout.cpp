
#include "dummy/swatch/Readout.hpp"


#include "dummy/Controller.hpp"


namespace dummy {
namespace swatch {

Readout::Readout(const Controller& aController) :
  ReadoutInterface(),
  mController(aController)
{
}


Readout::~Readout()
{
}


void Readout::retrieveMetricValues()
{
}

} // namespace swatch
} // namespace dummy
