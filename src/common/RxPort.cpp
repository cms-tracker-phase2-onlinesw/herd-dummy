
#include "dummy/swatch/RxPort.hpp"


#include "dummy/Controller.hpp"
#include "dummy/OpticsController.hpp"
#include "dummy/RxMGTStatus.hpp"

#include "swatch/core/MetricConditions.hpp"


namespace dummy {
namespace swatch {

using namespace ::swatch;


RxPort::RxPort(const size_t i, const Controller& aController, const phase2::ChannelID& aConnectedOpticsChannel) :
  InputPort(i, createId(i), aConnectedOpticsChannel),
  mController(aController),
  mPolarity(registerProperty<std::string>("Polarity")),
  mMgtType(registerProperty<std::string>("MGT type")),
  mRefClockIndex(registerProperty<uint16_t>("Ref clock index")),
  mPacketCount(registerMetric<uint16_t>("packetCount", "Packet count")),
  mHeaderErrorCount(registerMetric<uint16_t>("headerErrorCount", "Errors in headers", core::GreaterThanCondition<uint16_t>(0))),
  mFillerErrorCount(registerMetric<uint16_t>("fillerErrorCount", "Errors in filler words", core::GreaterThanCondition<uint16_t>(4), core::GreaterThanCondition<uint16_t>(0)))
{
}


RxPort::~RxPort()
{
}


OpticsController* RxPort::getOpticsController()
{
  return OpticsController::get(getConnection()->component);
}


std::string RxPort::createId(const size_t aIndex)
{
  std::ostringstream sstream;
  sstream << "Rx" << std::setw(2) << std::setfill('0') << aIndex;
  return sstream.str();
}


void RxPort::retrievePropertyValues()
{
  set<std::string>(mMgtType, getIndex() < 20 ? "GTH" : "GTY");
  set<std::string>(mPolarity, getIndex() % 2 ? "Inverted" : "Normal");
  set<uint16_t>(mRefClockIndex, getIndex() / 8);
}


void RxPort::retrieveMetricValues()
{
  const auto statusRegisters = mController.getRxPortStatus(getIndex());

  setMetric(mMetricIsLocked, statusRegisters.isLocked);
  setMetric(mMetricIsAligned, statusRegisters.isAligned);
  setMetric(mMetricCRCErrors, uint32_t(statusRegisters.crcErrorCount));

  setMetric(mMetricSourceChannelId, uint16_t(statusRegisters.sourceChannelId));
  setMetric(mMetricSourceSlotId, uint16_t(statusRegisters.sourceSlotId));
  setMetric(mMetricSourceCrateId, uint16_t(statusRegisters.sourceCrateId));

  setMetric(mPacketCount, statusRegisters.packetCount);
  setMetric(mHeaderErrorCount, statusRegisters.headerErrorCount);
  setMetric(mFillerErrorCount, statusRegisters.fillerErrorCount);
}


} // namespace swatch
} // namespace dummy
