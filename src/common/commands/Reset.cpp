
#include "dummy/swatch/commands/Reset.hpp"


#include "swatch/core/rules/IsAmong.hpp"
#include "swatch/phase2/InputPort.hpp"
#include "swatch/phase2/InputPortCollection.hpp"
#include "swatch/phase2/OutputPort.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"
#include "swatch/phase2/ReadoutInterface.hpp"
#include "swatch/phase2/TTCInterface.hpp"

#include "dummy/swatch/Processor.hpp"
#include "dummy/swatch/commands/utilities.hpp"


namespace dummy {
namespace swatch {
namespace commands {

using namespace ::swatch;
using namespace std::string_literals;


Reset::Reset(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "", "Configure clock and TTC source, and issue reset", aActionable, "Dummy command's default result!"s)
{
  registerDummyParameters(*this);
  registerParameter<std::string>("clockSource", "internal", core::rules::IsAmong<std::string>({ "internal", "external" }));
}

Reset::~Reset()
{
}

action::Command::State Reset::code(const core::ParameterSet& aParams)
{
  // Declare that only the TTC FW will be configured after this command
  using namespace ::swatch::phase2::processorIds;
  enableMonitoring(kTTC);
  disableMonitoring({ kReadout, kInputPorts, kOutputPorts });
  for (auto& port : getActionable<Processor>().getInputPorts().getPorts())
    disableMonitoring(kInputPorts + "." + port->getId());
  for (auto& port : getActionable<Processor>().getOutputPorts().getPorts())
    disableMonitoring(kOutputPorts + "." + port->getId());

  // [For UI tests] Parse dummy parameters and register some dummy messages
  const auto s = readDummyParametersAndPrintMessages(aParams, [&](float x, const std::string& m) { this->setProgress(x, m); });

  // Run the reset method of SWATCH-independent controller
  Controller& lController = getActionable<Processor>().getController();
  lController.reset();

  // [For UI tests] If command will fail, then for consistency, tell controller
  //                to return bad monitoring data values in future for RX ports
  if (s == action::Command::kError)
    lController.forceClkTtcHealth(ComponentHealth::kError);

  return s;
}


} // namespace commands
} // namespace swatch
} // namespace dummy
