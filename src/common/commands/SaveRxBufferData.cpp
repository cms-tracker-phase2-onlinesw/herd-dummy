
#include "dummy/swatch/commands/SaveRxBufferData.hpp"


#include <boost/filesystem.hpp>

#include "swatch/action/File.hpp"
#include "swatch/phase2/InputPort.hpp"
#include "swatch/phase2/InputPortCollection.hpp"

#include "dummy/swatch/Processor.hpp"
#include "dummy/swatch/commands/utilities.hpp"


namespace dummy {
namespace swatch {
namespace commands {

using namespace ::swatch;
using namespace std::string_literals;


SaveRxBufferData::SaveRxBufferData(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "", "Capture data from RX buffers and write to file", aActionable, "Dummy command's default result!"s)
{
  registerDummyParameters(*this);
}


SaveRxBufferData::~SaveRxBufferData()
{
}


action::Command::State SaveRxBufferData::code(const core::ParameterSet& aParams)
{
  // [For UI tests] Parse dummy parameters and register some dummy messages
  const auto s = readDummyParametersAndPrintMessages(aParams, [&](float x, const std::string& m) { this->setProgress(x, m); });

  // Return RX buffer data file from controller; if all MGTs in loopback return TX buffer data instead.
  bool lLoopback = true;
  for (const auto* lPort : getActionable<Processor>().getInputPorts().getPorts()) {
    if ((not lPort->isInLoopback()) and (not lPort->isMasked()))
      lLoopback = false;
  }

  const auto lController = getActionable<Processor>().getController();
  const std::string lFilePath = lLoopback ? lController.saveTxBuffers() : lController.saveRxBuffers();
  setResult(boost::any(action::File(lFilePath, ::swatch::phase2::fileTypes::kRxBufferData, "")));

  return s;
}


} // namespace commands
} // namespace swatch
} // namespace dummy
