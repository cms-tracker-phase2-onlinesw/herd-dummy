
#include "dummy/swatch/commands/ConfigureOpticalRx.hpp"


#include "swatch/core/rules/NonEmptyString.hpp"
#include "swatch/phase2/InputPort.hpp"
#include "swatch/phase2/InputPortCollection.hpp"

#include "dummy/OpticsController.hpp"
#include "dummy/swatch/Processor.hpp"
#include "dummy/swatch/RxPort.hpp"
#include "dummy/swatch/commands/utilities.hpp"
#include "dummy/swatch/utilities.hpp"


namespace dummy {
namespace swatch {
namespace commands {

using namespace ::swatch;
using namespace std::string_literals;


ConfigureOpticalRx::ConfigureOpticalRx(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, aActionable, "Dummy command's default result!"s)
{
  registerDummyParameters(*this);
  registerParameter<std::string>({ "ids"s, "List of channel indices" }, "0-99", core::rules::NonEmptyString<std::string>());
}


ConfigureOpticalRx::~ConfigureOpticalRx()
{
}


action::Command::State ConfigureOpticalRx::code(const core::ParameterSet& aParams)
{
  // [For UI tests] Parse dummy parameters and register some dummy messages
  const auto s = readDummyParametersAndPrintMessages(aParams, [&](float x, const std::string& m) { this->setProgress(x, m); });

  // Run the configureRx method of SWATCH-independent optics controller instances
  const std::vector<size_t> lIds(utilities::parseListOfIndices(aParams.get<std::string>("ids")));
  for (auto& port : getActionable<Processor>().getInputPorts().getPorts()) {
    auto* lOpticsController = dynamic_cast<RxPort&>(*port).getOpticsController();
    if ((std::count(lIds.begin(), lIds.end(), port->getIndex()) > 0) and port->isPresent() and lOpticsController) {
      lOpticsController->configureRx(port->getConnection()->index);

      // [For UI tests] If command will fail, then for consistency, tell controller
      //                to return bad monitoring data values in future for this RX channel
      if (s == action::Command::kError)
        lOpticsController->forceRxChannelHealth(port->getConnection()->index, ComponentHealth::kError);
    }
  }

  return s;
}


} // namespace commands
} // namespace swatch
} // namespace dummy
