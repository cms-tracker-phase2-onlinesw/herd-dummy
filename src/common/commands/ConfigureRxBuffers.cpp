
#include "dummy/swatch/commands/ConfigureRxBuffers.hpp"


#include <boost/filesystem.hpp>

#include "swatch/action/File.hpp"
#include "swatch/core/rules/NonEmptyString.hpp"

#include "dummy/swatch/Processor.hpp"
#include "dummy/swatch/commands/utilities.hpp"


namespace dummy {
namespace swatch {
namespace commands {

using namespace ::swatch;
using namespace std::string_literals;


ConfigureRxBuffers::ConfigureRxBuffers(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, aActionable, "Dummy command's default result!"s)
{
  registerDummyParameters(*this);
  registerParameter<std::string>({ "ids"s, "List of channel indices" }, "0-99", core::rules::NonEmptyString<std::string>());
  registerParameter<action::File>("dataFile", { "", "", "" });
}

ConfigureRxBuffers::~ConfigureRxBuffers()
{
}

action::Command::State ConfigureRxBuffers::code(const core::ParameterSet& aParams)
{
  // [For UI tests] Parse dummy parameters and register some dummy messages
  const auto s = readDummyParametersAndPrintMessages(aParams, [&](float x, const std::string& m) { this->setProgress(x, m); });

  // Copy data file to /tmp/swatch-dummy/rx_dummy.txt and set RX buffer data path in controller
  namespace fs = boost::filesystem;
  fs::create_directory(fs::path("/tmp/swatch-dummy"));
  const std::string lTargetFilePath("/tmp/swatch-dummy/rx_buffer.dat");
  fs::remove(fs::path(lTargetFilePath)); // Doesn't throw if file doesn't exist
  fs::copy(fs::path(aParams.get<action::File>("dataFile").getPath()), fs::path(lTargetFilePath));
  getActionable<Processor>().getController().configureRxBuffers(lTargetFilePath);

  return s;
}


} // namespace commands
} // namespace swatch
} // namespace dummy
