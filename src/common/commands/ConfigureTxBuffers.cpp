
#include "dummy/swatch/commands/ConfigureTxBuffers.hpp"


#include <boost/filesystem.hpp>

#include "swatch/action/File.hpp"
#include "swatch/core/rules/NonEmptyString.hpp"

#include "dummy/swatch/Processor.hpp"
#include "dummy/swatch/commands/utilities.hpp"


namespace dummy {
namespace swatch {
namespace commands {

using namespace ::swatch;
using namespace std::string_literals;


ConfigureTxBuffers::ConfigureTxBuffers(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, aActionable, "Dummy command's default result!"s)
{
  registerDummyParameters(*this);
  registerParameter<std::string>({ "ids"s, "List of channel indices" }, "0-79", core::rules::NonEmptyString<std::string>());
  registerParameter<action::File>("dataFile", { "", "", "" });
}

ConfigureTxBuffers::~ConfigureTxBuffers()
{
}

action::Command::State ConfigureTxBuffers::code(const core::ParameterSet& aParams)
{
  // [For UI tests] Parse dummy parameters and register some dummy messages
  const auto s = readDummyParametersAndPrintMessages(aParams, [&](float x, const std::string& m) { this->setProgress(x, m); });

  // Copy data file to /tmp/swatch-dummy/tx_dummy.txt and set TX buffer data path in controller
  namespace fs = boost::filesystem;
  fs::create_directory(fs::path("/tmp/swatch-dummy"));
  const std::string lTargetFilePath("/tmp/swatch-dummy/tx_buffer.dat");
  fs::remove(fs::path(lTargetFilePath)); // Doesn't throw if file doesn't exist
  fs::copy(fs::path(aParams.get<action::File>("dataFile").getPath()), fs::path(lTargetFilePath));
  getActionable<Processor>().getController().configureTxBuffers(lTargetFilePath);

  return s;
}


} // namespace commands
} // namespace swatch
} // namespace dummy
