
#include "dummy/swatch/commands/utilities.hpp"


#include <chrono>
#include <thread>

#include "swatch/action/Command.hpp"


namespace dummy {
namespace swatch {
namespace commands {

using namespace ::swatch;
using namespace std::string_literals;

void registerDummyParameters(action::Command& aCommand)
{
  aCommand.registerParameter("cmdDuration", float(1));
  aCommand.registerParameter<bool>({ "returnWarning"s, "Force dummy command to return warning" }, false);
  aCommand.registerParameter<bool>({ "returnError"s, "Force dummy command to return error" }, false);
  aCommand.registerParameter<bool>({ "throw"s, "Force dummy command to throw" }, false);
}


action::Command::State readDummyParametersAndPrintMessages(const core::ParameterSet& aParams, const std::function<void(float, const std::string&)> aSetProgress)
{
  if (aParams.get<bool>("throw"))
    throw core::RuntimeError("An exceptional error occurred!");

  action::Command::State lState = action::Command::kDone;
  if (aParams.get<bool>("returnError"))
    lState = action::Command::kError;
  else if (aParams.get<bool>("returnWarning"))
    lState = action::Command::kWarning;

  const size_t lNrMilliseconds = 1000 * aParams.get<float>("cmdDuration");
  const size_t lSleepDuration = 200;
  const size_t lNrSleeps = (lNrMilliseconds / lSleepDuration) + ((lNrMilliseconds % lSleepDuration) > 0 ? 1 : 0);
  for (size_t i = 0; i < lNrSleeps; i++) {
    std::this_thread::sleep_for(std::chrono::milliseconds(std::min(lSleepDuration, lNrMilliseconds - i * lSleepDuration)));
    std::ostringstream lMsg;
    lMsg << "Done " << i + 1 << " of " << lNrSleeps << " things";
    aSetProgress(float(i) / float(lNrSleeps), lMsg.str());
  }
  if (lNrSleeps == 0)
    aSetProgress(1.0, "Nothing to do.");

  return lState;
}

} // namespace commands
} // namespace swatch
} // namespace dummy
