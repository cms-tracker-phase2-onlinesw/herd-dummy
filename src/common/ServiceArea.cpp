
#include "dummy/swatch/ServiceArea.hpp"


#include "swatch/core/Factory.hpp"

#include "dummy/swatch/utilities.hpp"


SWATCH_REGISTER_CLASS(dummy::swatch::ServiceArea)


namespace dummy {
namespace swatch {

ServiceArea::ServiceArea(const ::swatch::core::AbstractStub& aStub) :
  ServiceModule(aStub)
{
}


ServiceArea::~ServiceArea()
{
}


void ServiceArea::retrieveMetricValues()
{
  if (utilities::isCardInShelf()) {
    setMetric(mMetricLogicalSlot, utilities::getLogicalSlot());
    setMetric(mMetricShelfAddress, utilities::getShelfAddress());
  }
  else {
    setMetric(mMetricLogicalSlot, uint16_t(0xF));
    setMetric(mMetricShelfAddress, std::string(""));
  }
}


} // namespace swatch
} // namespace dummy
