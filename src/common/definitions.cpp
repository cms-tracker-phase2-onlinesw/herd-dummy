
#include "dummy/definitions.hpp"

#include <iostream>


namespace dummy {

std::ostream& operator<<(std::ostream& aStream, const ComponentHealth& aHealth)
{
  switch (aHealth) {
    case ComponentHealth::kGood:
      return (aStream << "good");
    case ComponentHealth::kWarning:
      return (aStream << "warning");
    case ComponentHealth::kError:
      return (aStream << "error");
    case ComponentHealth::kNotReachable:
      return (aStream << "not-reachable");
  }

  return aStream;
}

} // namespace dummy
