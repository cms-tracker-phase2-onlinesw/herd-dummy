
#ifndef __DUMMY_TXMGTSTATUS_HPP__
#define __DUMMY_TXMGTSTATUS_HPP__


namespace dummy {

struct TxMGTStatus {
  bool isOperating { true };
  bool qpllLocked { true };
};

} // namespace dummy


#endif /* __DUMMY_TXMGTSTATUS_HPP__ */
