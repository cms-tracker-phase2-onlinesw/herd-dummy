
#ifndef __DUMMY_SWATCH_COMMANDS_SAVETXBUFFERDATA_HPP__
#define __DUMMY_SWATCH_COMMANDS_SAVETXBUFFERDATA_HPP__


#include "swatch/action/Command.hpp"

#include "dummy/definitions.hpp"


namespace dummy {
namespace swatch {
namespace commands {


class SaveTxBufferData : public ::swatch::action::Command {
public:
  SaveTxBufferData(const std::string& aId, ::swatch::action::ActionableObject& aActionable);
  ~SaveTxBufferData();

private:
  State code(const ::swatch::core::ParameterSet& aParams);
};


} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_SAVETXBUFFERDATA_HPP__ */
