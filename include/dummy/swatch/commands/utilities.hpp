
#ifndef __DUMMY_SWATCH_COMMANDS_UTILITIES_HPP__
#define __DUMMY_SWATCH_COMMANDS_UTILITIES_HPP__


#include "swatch/action/Command.hpp"


namespace dummy {
namespace swatch {
namespace commands {

void registerDummyParameters(::swatch::action::Command&);

::swatch::action::Command::State readDummyParametersAndPrintMessages(const ::swatch::core::ParameterSet&, const std::function<void(float, const std::string&)>);

} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_UTILITIES_HPP__ */
