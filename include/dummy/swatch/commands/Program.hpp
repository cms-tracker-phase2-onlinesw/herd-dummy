
#ifndef __DUMMY_SWATCH_COMMANDS_PROGRAM_HPP__
#define __DUMMY_SWATCH_COMMANDS_PROGRAM_HPP__


#include "swatch/action/Command.hpp"


namespace dummy {
namespace swatch {
namespace commands {


class Program : public ::swatch::action::Command {
public:
  Program(const std::string& aId, ::swatch::action::ActionableObject& aActionable);
  ~Program();

private:
  State code(const ::swatch::core::ParameterSet& aParams);
};


} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_PROGRAM_HPP__ */
