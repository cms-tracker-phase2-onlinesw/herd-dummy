
#ifndef __DUMMY_SWATCH_COMMANDS_CONFIGURETXMGTS_HPP__
#define __DUMMY_SWATCH_COMMANDS_CONFIGURETXMGTS_HPP__


#include "swatch/action/Command.hpp"


namespace dummy {
namespace swatch {
namespace commands {


class ConfigureTxMGTs : public ::swatch::action::Command {
public:
  ConfigureTxMGTs(const std::string& aId, ::swatch::action::ActionableObject& aActionable);
  ~ConfigureTxMGTs();

private:
  State code(const ::swatch::core::ParameterSet& aParams);
};


} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_CONFIGURETXMGTS_HPP__ */
