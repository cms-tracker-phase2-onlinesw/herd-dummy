
#ifndef __DUMMY_SWATCH_COMMANDS_CONFIGURETXBUFFERS_HPP__
#define __DUMMY_SWATCH_COMMANDS_CONFIGURETXBUFFERS_HPP__


#include "swatch/action/Command.hpp"


namespace dummy {
namespace swatch {
namespace commands {


class ConfigureTxBuffers : public ::swatch::action::Command {
public:
  ConfigureTxBuffers(const std::string& aId, ::swatch::action::ActionableObject& aActionable);
  ~ConfigureTxBuffers();

private:
  State code(const ::swatch::core::ParameterSet& aParams);
};


} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_CONFIGURETXBUFFERS_HPP__ */
