
#ifndef __DUMMY_SWATCH_COMMANDS_FORCETXPORTHEALTH_HPP__
#define __DUMMY_SWATCH_COMMANDS_FORCETXPORTHEALTH_HPP__


#include "swatch/action/Command.hpp"

#include "dummy/Controller.hpp"


namespace dummy {
namespace swatch {
namespace commands {


class ForceTxPortHealth : public ::swatch::action::Command {
public:
  ForceTxPortHealth(const std::string& aId, ::swatch::action::ActionableObject& aActionable);
  ~ForceTxPortHealth();

private:
  State code(const ::swatch::core::ParameterSet& aParams);

  static const std::map<std::string, ComponentHealth> kComponentHealthMap;
};


} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_FORCETXPORTHEALTH_HPP__ */
