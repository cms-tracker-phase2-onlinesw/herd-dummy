
#ifndef __DUMMY_SWATCH_COMMANDS_RESETOPTICALMODULES_HPP__
#define __DUMMY_SWATCH_COMMANDS_RESETOPTICALMODULES_HPP__


#include "swatch/action/Command.hpp"


namespace dummy {
namespace swatch {
namespace commands {


class ResetOpticalModules : public ::swatch::action::Command {
public:
  ResetOpticalModules(const std::string& aId, ::swatch::action::ActionableObject& aActionable);
  ~ResetOpticalModules();

private:
  State code(const ::swatch::core::ParameterSet& aParams);
};


} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_RESETOPTICALMODULES_HPP__ */
