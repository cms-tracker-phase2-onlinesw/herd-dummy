
#ifndef __DUMMY_SWATCH_COMMANDS_SAVERXBUFFERDATA_HPP__
#define __DUMMY_SWATCH_COMMANDS_SAVERXBUFFERDATA_HPP__


#include "swatch/action/Command.hpp"

#include "dummy/definitions.hpp"


namespace dummy {
namespace swatch {
namespace commands {


class SaveRxBufferData : public ::swatch::action::Command {
public:
  SaveRxBufferData(const std::string& aId, ::swatch::action::ActionableObject& aActionable);
  ~SaveRxBufferData();

private:
  State code(const ::swatch::core::ParameterSet& aParams);
};


} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_SAVERXBUFFERDATA_HPP__ */
