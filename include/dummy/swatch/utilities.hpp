#ifndef __DUMMY_SWATCH_UTILITIES_HPP__
#define __DUMMY_SWATCH_UTILITIES_HPP__


#include <stddef.h>
#include <string>
#include <vector>


namespace dummy {
namespace swatch {
namespace utilities {

bool isCardInShelf();

std::string getShelfAddress();

uint16_t getLogicalSlot();

std::vector<size_t> parseListOfIndices(const std::string&);

}
}
}

#endif /* __DUMMY_SWATCH_UTILITIES_HPP__ */
