
#ifndef __DUMMY_SWATCH_PROCESSOR_HPP__
#define __DUMMY_SWATCH_PROCESSOR_HPP__


#include "dummy/Controller.hpp"

#include "swatch/action/Property.hpp"
#include "swatch/action/Table.hpp"
#include "swatch/phase2/Processor.hpp"


namespace dummy {
namespace swatch {


class Processor : public ::swatch::phase2::Processor {
public:
  Processor(const ::swatch::core::AbstractStub& aStub);
  ~Processor();

  Controller& getController();

  void retrievePropertyValues();

  void retrieveMetricValues();

private:
  Controller mController;

  ::swatch::action::Property<std::string>& mBuildInfoType;
  ::swatch::action::Property<::swatch::action::Table>& mBuildInfoSourceArea;
  ::swatch::action::Property<std::string>& mBuildInfoTimestamp;

  static const size_t kNumRxPorts { 136 };
  static const size_t kNumTxPorts { 112 };
};


} // namespace swatch
} // namespace dummy


#endif /* __DUMMY_SWATCH_PROCESSOR_HPP__ */
