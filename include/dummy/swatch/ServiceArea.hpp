
#ifndef __DUMMY_SWATCH_SERVICEAREA_HPP__
#define __DUMMY_SWATCH_SERVICEAREA_HPP__


#include "swatch/phase2/ServiceModule.hpp"


namespace dummy {
namespace swatch {

class ServiceArea : public ::swatch::phase2::ServiceModule {
public:
  ServiceArea(const ::swatch::core::AbstractStub& aStub);
  ~ServiceArea();

private:
  void retrieveMetricValues();
};

} // namespace swatch
} // namespace dummy


#endif /* __DUMMY_SWATCH_SERVICEAREA_HPP__ */