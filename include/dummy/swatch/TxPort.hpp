
#ifndef __DUMMY_SWATCH_TXPORT_HPP__
#define __DUMMY_SWATCH_TXPORT_HPP__


#include "swatch/core/SimpleMetric.hpp"
#include "swatch/phase2/OutputPort.hpp"


namespace dummy {

class Controller;
class OpticsController;

namespace swatch {

class TxPort : public ::swatch::phase2::OutputPort {
public:
  TxPort(const size_t, const Controller&, const ::swatch::phase2::ChannelID&);
  ~TxPort();

  OpticsController* getOpticsController();

private:
  static std::string createId(const size_t);

  void retrievePropertyValues() final;

  void retrieveMetricValues() final;

  const Controller& mController;

  Property<std::string>& mPolarity;
  Property<std::string>& mMgtType;
  Property<uint16_t>& mRefClockIndex;
  Property<float>& mAmplitude;

  SimpleMetric<bool>& mMetricQpllLocked;
};

} // namespace swatch
} // namespace dummy


#endif /* __DUMMY_SWATCH_RXPORT_HPP__ */