
#ifndef __DUMMY_SWATCH_RXPORT_HPP__
#define __DUMMY_SWATCH_RXPORT_HPP__


#include "swatch/core/SimpleMetric.hpp"
#include "swatch/phase2/InputPort.hpp"


namespace dummy {

class Controller;
class OpticsController;

namespace swatch {

class RxPort : public ::swatch::phase2::InputPort {
public:
  RxPort(const size_t, const Controller&, const ::swatch::phase2::ChannelID&);
  ~RxPort();

  OpticsController* getOpticsController();

private:
  static std::string createId(const size_t);

  void retrievePropertyValues() final;

  void retrieveMetricValues() final;

  const Controller& mController;

  Property<std::string>& mPolarity;
  Property<std::string>& mMgtType;
  Property<uint16_t>& mRefClockIndex;

  SimpleMetric<uint16_t>& mPacketCount;
  SimpleMetric<uint16_t>& mHeaderErrorCount;
  SimpleMetric<uint16_t>& mFillerErrorCount;
};

} // namespace swatch
} // namespace dummy


#endif /* __DUMMY_SWATCH_RXPORT_HPP__ */