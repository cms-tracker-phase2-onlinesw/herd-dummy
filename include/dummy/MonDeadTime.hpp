#ifndef __DUMMY_MONDEADTIME_HPP__
#define __DUMMY_MONDEADTIME_HPP__

#include <random>

namespace dummy {
namespace mondeadtimeconfig {

// Define the structure of DeadTimes
// Max and Min for Uniform Distribution
template <typename T>
struct DeadTime {
  T min;
  T max;
};

bool isFastUpdate();
void monitoring_update_sleep(const DeadTime<float>&, std::mt19937&);

const DeadTime<float> kDeadTimeOpticalModuleBaseinNS = isFastUpdate() ? DeadTime<float> { 1.0e6, 2.0e6 } : DeadTime<float> { 1.0e8, 2.0e8 };
const DeadTime<float> kDeadTimeOpticalModuleRxinNS = isFastUpdate() ? DeadTime<float> { 1.0e2, 2.0e2 } : DeadTime<float> { 1.0e4, 2.0e4 };
const DeadTime<float> kDeadTimeTTCinNS = isFastUpdate() ? DeadTime<float> { 5.0e3, 1.5e4 } : DeadTime<float> { 5.0e5, 1.5e6 };
const DeadTime<float> kDeadTimeRxPortinNS = isFastUpdate() ? DeadTime<float> { 3.5e3, 7.5e3 } : DeadTime<float> { 3.5e5, 7.5e5 };
const DeadTime<float> kDeadTimeTxPortinNS = isFastUpdate() ? DeadTime<float> { 1.0e3, 3.0e3 } : DeadTime<float> { 1.0e5, 3.0e5 };
const DeadTime<float> kDeadTimeAlgoinNS = isFastUpdate() ? DeadTime<float> { 4.0e2, 5.0e2 } : DeadTime<float> { 4.0e4, 5.0e4 };
const DeadTime<float> kDeadTimeOpticalModuleTxinNS = isFastUpdate() ? DeadTime<float> { 5.0e1, 1.5e2 } : DeadTime<float> { 5.0e3, 1.5e4 };

} // end namespace mondeadtimeconfig
} // end namesapce dummy
#endif /* __DUMMY_MONDEADTIME_HPP__ */
