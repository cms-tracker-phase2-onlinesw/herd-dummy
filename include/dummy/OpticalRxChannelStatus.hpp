
#ifndef __DUMMY_OPTICALRXCHANNELSTATUS_HPP__
#define __DUMMY_OPTICALRXCHANNELSTATUS_HPP__


namespace dummy {

struct OpticalRxChannelStatus {
  float power { 42.0 };
  bool lossOfLock { false };
  bool lossOfSignal { false };
};

} // namespace dummy


#endif /* __DUMMY_OPTICALRXCHANNELSTATUS_HPP__ */
