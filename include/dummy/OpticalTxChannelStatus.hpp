
#ifndef __DUMMY_OPTICALTXCHANNELSTATUS_HPP__
#define __DUMMY_OPTICALTXCHANNELSTATUS_HPP__


namespace dummy {

struct OpticalTxChannelStatus {
  bool lossOfLock { false };
};

} // namespace dummy


#endif /* __DUMMY_OPTICALTXCHANNELSTATUS_HPP__ */
