
#ifndef __DUMMY_DEFINITIONS_HPP__
#define __DUMMY_DEFINITIONS_HPP__


#include <iosfwd>


namespace dummy {

enum class ComponentHealth {
  kGood,
  kWarning,
  kError,
  kNotReachable
};

std::ostream& operator<<(std::ostream&, const ComponentHealth&);

enum class Direction {
  kRx,
  kTx
};

} // namespace dummy


#endif /* __DUMMY_DEFINITIONS_HPP__ */
